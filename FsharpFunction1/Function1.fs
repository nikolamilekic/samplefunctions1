namespace FunctionApp1

open System
open System.IO
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Logging
open Newtonsoft.Json
open System
open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Host
open System.Net
open System.Net.Http
//open FSharp.Interop.Dynamic

module Function1 =

    [<FunctionName("Function1")>]
    let Run(
        [<HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)>]
        req: HttpRequestMessage) =
        async {
            return "TEST456!"
        } |> Async.StartAsTask
